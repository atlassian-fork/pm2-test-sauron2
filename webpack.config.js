const path = require('path');
const glob = require('glob');

const buildEntries = entrypath => {
    const files = glob.sync(entrypath);
    const result = files.reduce(function(map, filepath) {
        map[path.parse(filepath).name] = filepath;
        return map;
    }, {});
    return result;
};

module.exports = {
    mode: 'development',
    resolve: {
        extensions: ['.js', '.ts'],
    },
    module: {
        rules: [{
                test: /\.ts$/,
                exclude: /node_modules/,
                use: {
                    loader: 'ts-loader',
                },
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },
        ],
    },
    entry: buildEntries('./!(*.config).{js,ts}'),
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js',
    },
    target: 'node',
};