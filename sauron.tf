provider "pollinator" {
  version = "= 1.2.1"
  envtype = "${var.pollinator_envtype}"
  team    = "${var.pollinator_team}"
  username = "${var.username}"
  password = "${var.password}"
}

terraform {
  required_version = "~> 0.11.13"

  backend "http" {
    address = "https://sauron.prod.atl-paas.net/terraform/atlassian/pm2-test-sauron"
  }
}